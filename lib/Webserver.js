import express from 'express';

export class Webserver {
	constructor(config) {
		this.app = express();
		this.addRoutes();
		this.app.listen(config.port);
		this.imageCache;
	}

	addRoutes() {
		this.app.get('/', (req, res) => {
			res.type('html');
			res.setHeader('Refresh', '1');
			res.end('ohai!<br><img src="/image" alt="pixelflut random image here, no actual content">');
		});

		this.app.get('/image', (req, res) => {
			res.type('png');
			res.end(this.imageCache);
		});
	}

	updateImageCache(data) {
		this.imageCache = data;
	}
}
