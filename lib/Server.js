import { createServer } from 'net';
import EventEmitter from 'events';

import { StreamInterpreter } from './StreamInterpreter.js';
import { logger } from './logger.js';

export class Server extends EventEmitter {
	constructor(port = 1234) {
		super();

		this.streamCounter = 0;
		this.streams = [];
		this.port = port;
		this.socket = createServer(c => this.addConnection(c));
		this.socket.on('close', (args) => this.emit('close', args));

		this.socket.listen(this.port, () => {
			logger.info(`Now listening on Port ${this.port}`);
			this.emit('listening');
		});
	}

	addConnection(connection) {
		this.streamCounter++;
		const si = new StreamInterpreter(this.streamCounter, connection, this);
		logger.info(`[${si.id}] + New Connection from ${connection.remoteAddress}:${connection.remotePort}`);
		this.streams.push(si);

		si.on('instruction', instruction => this.emit('instruction', instruction));
		si.once('close', () => this.removeConnection(si));
	}

	removeConnection(si) {
		logger.info(`[${si.id}] - Removed Connection`);
		const index = this.streams.indexOf(si);
		if (index < 0) {
			throw `connection ${si.id} was not found?!`;
		}
		this.streams.splice(index, 1);
		si.removeAllListeners('close');
		si.removeAllListeners('instruction');
	}

	close() {
		this.streams.forEach(s => {
			s.close();
		});
		this.socket.close();
	}
}
