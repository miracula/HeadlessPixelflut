import readline from 'readline';
import EventEmitter from 'events';

import { logger } from './logger.js';
import { Instruction } from './Instruction.js';

export class StreamInterpreter extends EventEmitter {
	constructor(id, stream) {
		super();
		this.id = id;
		this.stream = stream;

		this.rl = readline.createInterface({
			input: this.stream,
			output: this.stream
		});

		this.rl.on('line', (line) => {
			const instruction = Instruction.fromLine(line);
			if (instruction) {
				this.emit('instruction', instruction);
			} else {
				logger.warn(`could not decipher message ${line}`);
			}
		});

		this.stream.on('error', (err) => {
			logger.info(`[${this.id}] Error in stream. Reason: ${err}`);
		});
		this.stream.on('close', (args) => {
			logger.info(`[${this.id}] - stream closed.`);
			this.emit('close', args);
			this.close();
		});
	}

	close() {
		if (this.stream) { this.stream.end(); }
		if (this.rl) { this.rl.close(); }
		this.stream = null;
		this.rl = null;
	}
}
