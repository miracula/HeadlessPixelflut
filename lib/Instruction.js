export const FORMAT_PARSER = /^PX\s+(?<x>\d+)\s+(?<y>\d+)\s+(?<r>[0-9a-f]{2})(?<g>[0-9a-f]{2})(?<b>[0-9a-f]{2})\s*$/i;

export class Instruction {
	static fromLine(input) {
		const result = input.match(FORMAT_PARSER);
		if (!result) return null;
		const groups = result.groups;
		return new Instruction(parseInt(groups.x, 10), parseInt(groups.y, 10), parseInt(groups.r, 16), parseInt(groups.g, 16), parseInt(groups.b, 16));
	}

	static fromObject(input) {
		return new Instruction(parseInt(input.x, 10), parseInt(input.y, 10), input.color);
	}

	constructor(x, y, r, g, b) {
		this.x = x;
		this.y = y;
		this.r = r;
		this.g = g;
		this.b = b;
	}

	toObject() {
		return {
			x: this.x,
			y: this.y,
			r: this.r,
			g: this.g,
			b: this.b
		};
	}

	toString() {
		return `X: ${this.x}, Y: ${this.y}, color: ${this.r}${this.g}${this.b}`; // TODO leading zeros.
	}
}
