# HeadlessPixelflut

Pixelflut is great, but if you can't be in the same room together, you need a headless version!
Implementation of the [Pixelflut protocol](https://wiki.cccgoe.de/wiki/Pixelflut) in JavaScript for rendering to an image canvas.


## Requirements

Node.JS+npm


## Setup

Clone this repository
Update `config.js` (copy over from `config.js.example`)
You may need to open up a port in your firewall. Default Pixelflut port is `1234`. There's a rulefile for `ufw` in `/tools`. Enable with `ufw allow HeadlessPixelflut`.
run `npm start` or set this up as a service.


## How to use

Connect to port 1234/tcp
Write commands in this format:

    PX 20 30 ff8800\n

That is:

- `PX` (always this very string)
- white space
- digits for the x-coordinate
- white space
- digits for the y-coordinate
- white space
- up to eight hexadecimal digits, these will be interpreted as rrggbbaa.
- newline character separates individual commands. If this is `\r\n` it's also fine, we just ignore the `\r`.


## How to develop

Make sure to always run the tests via `npm run test`.
