import path from 'path';

import { Canvas } from './lib/Canvas.js';
import { logger } from './lib/logger.js';
import { Server } from './lib/Server.js';
import { Webserver } from './lib/Webserver.js';

import config from './config.js';

logger.info('HeadlessPixelflut starting');

const canvas = new Canvas(config.canvas.width, config.canvas.height);
const server = new Server(config.pixelflut.port);
const webserver = new Webserver(config.webserver);

canvas.fill(0, 0, 0, 255);

server.on('instruction', (instruction) => {
	canvas.draw(instruction);
});

setInterval(async () => {
	const filename = path.resolve(`public/stored/${(new Date).getTime()}.png`);

	logger.info(`storing current image in ${filename}`);
	await canvas.store(filename);
}, config.storageInterval);

setInterval(async () => {
	webserver.updateImageCache(canvas.getPNG());
}, config.webserverViewInterval);
