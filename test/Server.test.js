import assert from 'assert';
import { Socket } from 'net';

import { describe, it, before, after } from 'mocha';
import winston from 'winston';

import { logger } from '../lib/logger.js';
import { Server } from '../lib/Server.js';

// silences all the log messages from Server.
logger.clear().add(new winston.transports.Console({level: 'warn'}));

describe('Server', () => {
	let counter = 40000;

	describe('#constructor', () => {
		it('runs', () => {
			const s = new Server(counter++);
			s.socket.close();
			assert(s);
		});
	});

	describe('#addConnection and #removeConnection', () => {
		let s;
		let addrinfo;

		before((done) => {
			s = new Server(counter++);
			s.once('listening', () => {
				addrinfo = s.socket.address();
				done();
			});
		});

		after((done) => {
			s.once('close', () => {
				done();
			});
			s.close();
		});

		it('accepts and removes one connections', (done) => {
			const client = new Socket();
			client.connect(addrinfo.port, addrinfo.address, () => {
				assert.strictEqual(s.streams.length, 1);
				client.once('close', () => {
					assert.strictEqual(s.streams.length, 0);
					done();
				});
				client.end();
			});
		});

		it('it handles numerous connections', (done) => {
			const CLIENT_NUMBER = 250;
			const MESSAGES_PER_CLIENT = 10;
			let started = 0;
			let stopped = 0;
			let instructionCounter = 0;

			s.on('instruction', () => { instructionCounter++; });

			for(let i = 0; i < CLIENT_NUMBER; i++) {
				const client = new Socket();
				client.connect(addrinfo.port, addrinfo.eddress, () => {
					started++;
					if (started === CLIENT_NUMBER) {
						assert.strictEqual(s.streams.length, CLIENT_NUMBER);
					}
					for (let i = 0; i < MESSAGES_PER_CLIENT; i++) {
						client.write('PX 0 0 000000\n');
					}
					client.end();
				});
				client.once('close', () => {
					stopped++;
					if (stopped === CLIENT_NUMBER) {
						setTimeout(() => {
							assert.strictEqual(s.streams.length, 0);
							assert.strictEqual(instructionCounter, CLIENT_NUMBER * MESSAGES_PER_CLIENT);
							done();
						}, 10);
					}
				});
			}
		});

		it('gracefully closes client connections on server stop', (done) => {
			const CLIENT_NUMBER = 250;
			let started = 0;
			let stopped = 0;

			for(let i = 0; i < CLIENT_NUMBER; i++) {
				const client = new Socket();
				client.connect(addrinfo.port, addrinfo.eddress, () => {
					started++;
					if (started === CLIENT_NUMBER) {
						s.close();
					}
				});
				client.once('close', () => {
					stopped++;
					if (stopped === CLIENT_NUMBER) {
						setTimeout(() => {
							assert.strictEqual(s.streams.length, 0);
							done();
						});
					}
				});
			}
		});
	});
});
