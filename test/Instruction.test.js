import assert from 'assert';
import { describe, it } from 'mocha';
import { Instruction } from '../lib/Instruction.js';

describe('Instruction', () => {
	describe('#constructor', () => {
		it('creates an object with expected values', () => {
			const instruction = new Instruction(5, 10, 11, 22, 33);
			assert.strictEqual(instruction.x, 5);
			assert.strictEqual(instruction.y, 10);
			assert.strictEqual(instruction.r, 11);
			assert.strictEqual(instruction.g, 22);
			assert.strictEqual(instruction.b, 33);
		});
	});

	describe('#fromLine', () => {
		it('should parse a string in an expected manner', () => {
			const instruction = Instruction.fromLine('PX 22 33 112233');
			const expected = new Instruction(22, 33, 0x11, 0x22, 0x33);
	
			assert.strictEqual(instruction.x, expected.x);
			assert.strictEqual(instruction.y, expected.y);
			assert.strictEqual(instruction.r, expected.r);
			assert.strictEqual(instruction.g, expected.g);
			assert.strictEqual(instruction.b, expected.b);
		});
	
		it('should not return an instruction if it\'s not the right format', () => {
			const instruction = Instruction.fromLine('PX000000');
			assert.strictEqual(instruction, null);
		});
	});
});
