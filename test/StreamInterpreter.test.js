import assert from 'assert';
import { describe, it } from 'mocha';

import MemoryStream from 'memorystream';

import { StreamInterpreter } from '../lib/StreamInterpreter.js';

describe('StreamInterpreter', () => {
	describe('#constructor', () => {
		it('sets up a linereader', () => {
			const input = MemoryStream(['']);
			const si = new StreamInterpreter('some-id', input);
			assert(si.rl);
			assert(si.stream);
		});
	});

	describe('#on instruction event', () => {
		it('fires for an instruction', (done) => {
			const input = MemoryStream();
			const si = new StreamInterpreter('some-id', input);
			si.on('instruction', () => {
				assert(true);
				done();
			});
			input.write('PX 0 0 c0ffee\n');
		});
	});
});
