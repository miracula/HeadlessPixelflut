# follows https://nodejs.org/en/docs/guides/nodejs-docker-webapp/
FROM node:15
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm ci --only=production
COPY . .
EXPOSE 8080/tcp
EXPOSE 1234/tcp
CMD [ "node", "index.js" ] 
