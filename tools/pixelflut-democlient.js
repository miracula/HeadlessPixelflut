import net from 'net';

const PORT = '1234';
const IP = 'localhost';

const client = new net.Socket();

// set port and IP to the correct values (see slides)
client.connect(PORT, IP, function () {
	console.log('connected');

	client.write('SIZE\n');
	setInterval(() => {
		client.write(`PX ${Math.floor(Math.random()*200)} ${Math.floor(Math.random()*200)} ff0000\n`);
	}, 5);
});

client.on('data', function (data) {
	console.log('incoming %s', data);
});
