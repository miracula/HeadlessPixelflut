import { Canvas } from '../lib/Canvas.js';
import { StreamInterpreter } from '../lib/StreamInterpreter.js';

const WIDTH = 640;
const HEIGHT = 360;

const canvas = new Canvas(WIDTH, HEIGHT);
canvas.fill(0, 0, 0, 255);
const interpreter = new StreamInterpreter('console', process.stdin);

interpreter.on('instruction', (instruction) => {
	console.log(instruction);
	canvas.draw(instruction);
});

interpreter.rl.on('close', async () => {
	console.log('now closing');
	await canvas.store('./stdinput.png');
	console.log('all done. bye.');
});
